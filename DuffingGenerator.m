% This code was created by Joel Rosenfeld in 2021 to accompany his YouTube
% channel ThatMaththing (http://www.thatmaththing.com/
% If you use this code for a project, please credit Joel A. Rosenfeld, and
% link his YouTube channel and professional website,
% http://www.thelearningdock.org/

% This will give us the trajectories for the Duffing Oscillator
f = @(x) [ x(2); x(1) - x(1)^3]; % Duffing Oscilator Dynamics

% We are going sample in the region [-3,3]x[-3,3].

x_sample = -3:0.5:3;

[X,Y] = meshgrid(x_sample);

X = reshape(X,[],1);
Y = reshape(Y,[],1);

xy_samples = [X,Y];
xy_samples = xy_samples';

h = 0.01;
T = 1;

% TrajHolder - First Entry indexes our Trajectories, Second is Dimension,
% and Third is Time. First Trajectory is given as
% squeeze(TrajectoryHolder(1,:,:));
TrajectoryHolder = zeros(size(xy_samples,2),2,T/h);

for i = 1:size(xy_samples,2)
    TrajectoryHolder(i,:,:) = rk4method(xy_samples(:,i),f,h,T);
end