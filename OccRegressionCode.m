% This code was created by Joel Rosenfeld in 2021 to accompany his YouTube
% channel ThatMaththing (http://www.thatmaththing.com/
% If you use this code for a project, please credit Joel A. Rosenfeld, and
% link his YouTube channel and professional website,
% http://www.thelearningdock.org/

tic
% Make a Gram Matrix
    load('BunchOfDuffings'); % Gives h and W. Where is the collection of trajectories

    W = W(1:1:end,:,:);
    TotalTrajectories = size(W,1);
    Dimension = size(W,2);
    TotalSnapshots = size(W,3);
    
    if(mod(TotalSnapshots,2) ~= 1)
        W = W(:,:,1:end-1);
        TotalSnapshots = TotalSnapshots - 1;
    end
    
    SimpsonsRuleVector = [1,3+(-1).^(0:TotalSnapshots-3),1]';
    
% Select a Kernel
    K = @(x,y) exp(x'*y/mu);
    mu = 5;
    
% Regularization Parameter
    lambda = 0.001;
    
% Gram Matrix - 2D matrix that is of size NumberOfTraj by NumberOfTraj

    GramMatrix = zeros(TotalTrajectories);
    
    for i = 1:TotalTrajectories
        for j = 1:TotalTrajectories
            Traj1 = squeeze(W(i,:,:));
            Traj2 = squeeze(W(j,:,:));
            
            GramMatrix(i,j) = h^2/9*SimpsonsRuleVector'*exp(1/mu*Traj1'*Traj2)*SimpsonsRuleVector;
        end
    end
    
    OuterSum = zeros(size(GramMatrix));
    
    for i = 1:TotalTrajectories
       OuterSum = OuterSum + GramMatrix(:,i)*GramMatrix(:,i)'; 
    end
    
% Differences of EndPoints
    EvalMatrix = zeros(TotalTrajectories,Dimension);
    
    for i = 1:TotalTrajectories
       EvalMatrix(i,:) = squeeze(W(i,:,end) - W(i,:,1))'; 
    end
    
% Weights
    Weights = pinv(OuterSum + lambda*GramMatrix)*GramMatrix*EvalMatrix; % Each column corresponds to weights for a different dimension.
    
    
% Occupation Kernel Evaluation

    %integral from 0 to T of exp(1/mu x^T gamma(t)) dt

    OccupationKernel = @(x,i) h/3*exp(1/mu * x'*squeeze(W(i,:,:)))*SimpsonsRuleVector;
    
    EvalApproximationHold = @(x) OccupationKernel(x,1);
    
    for i = 2:TotalTrajectories
        EvalApproximationHold = @(x) [EvalApproximationHold(x);OccupationKernel(x,i)];
    end
    
    EvalApproximation = @(x) Weights'*EvalApproximationHold(x); % Column Vector of size Dimension
    
% Plot Approximation

% Plotting Variables

x1 = -3:0.1:3;
[X,Y] = meshgrid(x1);
Z1 = zeros(size(X));
Z2 = zeros(size(X));

for i=1:length(x1)
    for j=1:length(x1)
        HoldMe = EvalApproximation([X(i,j);Y(i,j)]);
        Z1(i,j) = HoldMe(1);
        Z2(i,j) = HoldMe(2);
    end
end

figure
surf(X,Y,Z1);
title('First Dimension Approximation');

figure
surf(X,Y,Z2);
title('Second Dimension Approximation');

%Calculate Errors
f = @(x) [ x(2); x(1) - x(1)^3];  %Duffing Oscillator

x1 = -3:0.1:3;
[X,Y] = meshgrid(x1);
errZ1 = zeros(size(X));
errZ2 = zeros(size(X));

for i=1:length(x1)
    for j=1:length(x1)
        HoldMe = abs(EvalApproximation([X(i,j);Y(i,j)]) - f([X(i,j);Y(i,j)]));
        errZ1(i,j) = HoldMe(1);
        errZ2(i,j) = HoldMe(2);
    end
end

figure
surf(X,Y,errZ1);
title('First Dimension Error');

figure
surf(X,Y,errZ2);
title('Second Dimension Error');

toc
